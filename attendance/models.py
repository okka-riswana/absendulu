from django.core.files.storage import FileSystemStorage
from django.db import models
from django.conf import settings
from django.urls import reverse
import uuid

fs = FileSystemStorage(location=settings.MEDIA_ROOT)


class Group(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=50)
    description = models.TextField(max_length=200)
    code = models.CharField(max_length=50, blank=True, null=True, unique=True)
    members = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        related_name='members',
        blank=True
    )
    admins = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        related_name='admins',
        blank=True
    )

    def __str__(self):
        return f'{self.pk} - {self.name}'

    def get_absolute_url(self):
        return reverse('group_detail', args=[self.pk])

    def get_update_url(self):
        return reverse('group_update', args=[self.pk])

    def get_delete_url(self):
        return reverse('group_delete', args=[self.pk])


class Report(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=50)
    description = models.TextField(max_length=200)
    group = models.ForeignKey(
        Group,
        on_delete=models.CASCADE
    )

    def __str__(self):
        return f'{self.pk} - {self.name}'


class Record(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    photo = models.ImageField(
        storage=fs, upload_to='attendance_records', blank=True)
    time = models.DateTimeField(auto_now_add=True)
    location = models.CharField(max_length=100, blank=True)
    report = models.ForeignKey(
        Report, related_name='records', on_delete=models.CASCADE, blank=True)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return f'{self.pk} - {self.user.username}'
