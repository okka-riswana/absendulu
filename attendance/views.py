from django.views.generic import (
    CreateView,
    ListView,
    UpdateView,
    View,
    DetailView,
    FormView,
    TemplateView
)
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.shortcuts import redirect
from .models import Group, Record, Report
from .forms import GroupForm, ReportForm, RecordForm
from django.urls import reverse
from django.db.models import Q
import uuid
import base64
import io
from PIL import Image
from django.core.files.uploadedfile import InMemoryUploadedFile


class GroupCreateView(LoginRequiredMixin, CreateView):
    form_class = GroupForm
    template_name = 'attendance/group_form.html'

    def form_valid(self, form):
        instance = form.save(commit=False)
        instance.code = str(uuid.uuid4())
        instance.is_approved = True
        instance.save()
        instance.admins.add(self.request.user)
        return super().form_valid(form)


class GroupListView(ListView):
    model = Group
    template_name = 'attendance/group_list.html'

    def get_queryset(self):
        user = self.request.user
        queryset = super().get_queryset()
        queryset = queryset.filter(Q(admins=user) | Q(members=user))
        return queryset.prefetch_related('admins', 'members')


class GroupUpdateView(LoginRequiredMixin, UpdateView):
    model = Group
    form_class = GroupForm
    template_name = 'attendance/group_form.html'


class GroupDeleteView(LoginRequiredMixin, View):
    def post(self, request, **kwargs):
        group = Group.objects.get(pk=self.kwargs['group_pk'])
        report = Report.objects.filter(group=group)
        if report.exists():
            report.delete()
        group.delete()
        return redirect('group_list')

class GroupLeaveView(LoginRequiredMixin, View):
    def post(self, request, **kwargs):
        user = self.request.user
        group = Group.objects.get(pk=self.kwargs['group_pk'])
        group.members.remove(user)
        return redirect('group_list')

class ReportCreateView(LoginRequiredMixin, UserPassesTestMixin, CreateView):
    form_class = ReportForm
    template_name = 'attendance/report_form.html'

    def form_valid(self, form):
        group_pk = self.kwargs['group_pk']
        group = Group.objects.get(pk=group_pk)
        form.instance.group = group
        return super().form_valid(form)

    def test_func(self):
        group_pk = self.kwargs['group_pk']
        user = self.request.user
        admins = Group.objects.get(pk=group_pk).admins
        return admins.filter(pk=user.pk).exists()

    def get_success_url(self):
        group_pk = self.kwargs['group_pk']
        return reverse('report_list', args=[group_pk])


class ReportListView(LoginRequiredMixin, UserPassesTestMixin, ListView):
    model = Report
    template_name = 'attendance/report_list.html'

    def get_queryset(self):
        group_pk = self.kwargs['group_pk']
        queryset = super().get_queryset()
        queryset = queryset.prefetch_related('group').filter(group=group_pk)
        return queryset

    def test_func(self):
        group_pk = self.kwargs['group_pk']
        user = self.request.user
        admins = Group.objects.get(pk=group_pk).admins
        return admins.filter(pk=user.pk).exists()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        group_pk = self.kwargs['group_pk']
        context['group'] = Group.objects.get(pk=group_pk)
        return context


class MeetingListView(LoginRequiredMixin, UserPassesTestMixin, ListView):
    model = Report
    template_name = 'attendance/meeting_list.html'

    def get_queryset(self):
        group_pk = self.kwargs['group_pk']
        queryset = super().get_queryset()
        queryset = queryset.prefetch_related('group').filter(group=group_pk)
        return queryset

    def test_func(self):
        group_pk = self.kwargs['group_pk']
        user = self.request.user
        members = Group.objects.get(pk=group_pk).members
        return members.filter(pk=user.pk).exists()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        group_pk = self.kwargs['group_pk']
        user = self.request.user
        context['group'] = Group.objects.get(pk=group_pk)
        record_list = Record.objects.filter(
            user=user).values_list('report', flat=True)
        context['attended'] = record_list
        print(record_list)
        return context


class RecordCreateView(LoginRequiredMixin, FormView):
    model = Record
    form_class = RecordForm
    template_name = 'attendance/record_form.html'

    def post(self, request, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        report_pk = kwargs['report_pk']
        report = Report.objects.get(pk=report_pk)
        user = request.user
        _, img_data = str(request.POST.get('photo')).split(',')
        img = decodeImage(img_data)
        img_io = io.BytesIO()
        img.save(img_io, format='PNG')
        if form.is_valid():
            form.instance.photo = InMemoryUploadedFile(
                img_io,
                field_name=None,
                name=str(uuid.uuid4())+'.png',
                content_type='image/png',
                size=img_io.tell,
                charset=None)
            form.instance.report = report
            form.instance.user = user
            form.save()
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('group_list')


class JoinGroupView(LoginRequiredMixin, TemplateView):
    template_name = 'attendance/join_group.html'

    def post(self, request, **kwargs):
        code = request.POST['code']
        user = request.user
        group = Group.objects.get(code=code)
        group.members.add(user)
        return redirect(reverse('group_list'))


class RecordListView(LoginRequiredMixin, ListView):
    model = Record
    template_name = 'attendance/record_list.html'

    def get_queryset(self):
        report_pk = self.kwargs['report_pk']
        return super().get_queryset().filter(report=report_pk)


class GroupDetailView(LoginRequiredMixin, DetailView):
    model = Group
    template_name = 'attendance/group_detail.html'

    def get_queryset(self):
        return super().get_queryset().prefetch_related('members', 'admins')


class ReportDetailView(LoginRequiredMixin, ListView):
    model = Report


def decodeImage(data):
    try:
        data = base64.b64decode(data.encode('UTF-8'))
        buf = io.BytesIO(data)
        img = Image.open(buf)
        return img
    except IOError:
        return None
