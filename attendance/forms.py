from django import forms
from .models import Group, Report, Record


class GroupForm(forms.ModelForm):
    class Meta:
        model = Group
        fields = ['name', 'description']


class ReportForm(forms.ModelForm):
    class Meta:
        model = Report
        fields = ['name', 'description']


class RecordForm(forms.ModelForm):
    class Meta:
        model = Record
        fields = ['location']


class JoinGroupForm(forms.BaseForm):
    class Meta:
        fields = ['code']
