from django.contrib import admin
from .models import Group, Report, Record

admin.site.register(Group)
admin.site.register(Report)
admin.site.register(Record)
