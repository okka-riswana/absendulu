from django.urls import path
from .views import (
    GroupCreateView,
    GroupListView,
    GroupUpdateView,
    GroupDeleteView,
    JoinGroupView,
    ReportCreateView,
    ReportListView,
    RecordCreateView,
    GroupDetailView,
    MeetingListView,
    RecordListView,
    GroupLeaveView
)

urlpatterns = [
    path('groups', GroupListView.as_view(), name='group_list'),
    path('groups/create', GroupCreateView.as_view(), name='group_create'),
    path('groups/update/<slug:pk>',
         GroupUpdateView.as_view(), name='group_update'),
    path('groups/delete/<slug:group_pk>',
         GroupDeleteView.as_view(), name='group_delete'),
     path('groups/leave/<slug:group_pk>',
         GroupLeaveView.as_view(), name='group_leave'),
    path('groups/join',
         JoinGroupView.as_view(), name='group_join'),
    path('groups/<slug:group_pk>/reports/create',
         ReportCreateView.as_view(), name='report_create'),
    path('groups/<slug:group_pk>/reports',
         ReportListView.as_view(), name='report_list'),
    path('groups/<slug:group_pk>/meetings',
         MeetingListView.as_view(), name='meeting_list'),
    path('groups/<slug:pk>', GroupDetailView.as_view(), name='group_detail'),
    path('groups/attend/<slug:report_pk>',
         RecordCreateView.as_view(), name='record_create'),
    path('report/<slug:report_pk>',
         RecordListView.as_view(), name='record_list'),
]
