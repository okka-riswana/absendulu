from django.contrib.auth import get_user_model
from django.views.generic import ListView
from django.contrib.auth.mixins import LoginRequiredMixin

class ProfileView(LoginRequiredMixin, ListView):
    model = get_user_model()
    template_name = 'account/profile.html'

