from django.apps import AppConfig


class AccountConfig(AppConfig):
    name = 'account'
    label = 'absendulu_account'
